#!/usr/bin/env bash
# Take mobilenet v1 as example
# Download the pretrained model from
# https://storage.googleapis.com/download.tensorflow.org/models/mobilenet_v1_1.0_224_frozen.tgz
CKPT_PATH=./mobilenet_v1_1.0_224

freeze_graph --input_graph=${CKPT_PATH}/mobilenet_v1_1.0_224_eval.pbtxt \
             --input_checkpoint=${CKPT_PATH}/mobilenet_v1_1.0_224.ckpt \
             --output_node_names=MobilenetV1/Predictions/Reshape_1 \
             --output_graph=./mobilenet_v1_1.0_224.pb

# inference_input_type and inference_type can either FLOAT or QUANTIZED_UINT8

toco --graph_def_file=./mobilenet_v1_1.0_224.pb \
     --output_file=./mobilenet_v1_1.0_224.tflite \
     --output_format=TFLITE \
     --inference_input_type=FLOAT \
     --inference_type=FLOAT \
     --input_array=input \
     --output_array=MobilenetV1/Predictions/Reshape_1
