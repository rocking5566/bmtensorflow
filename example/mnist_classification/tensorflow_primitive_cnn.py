import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from tensorflow.examples.tutorials.mnist import input_data


def main():
    mnist = input_data.read_data_sets("MNIST_data", one_hot=True)

    # check MNIST training images matrix shape
    print(mnist.train.images.shape)
    print(mnist.train.labels.shape)
    print(mnist.test.images.shape)
    print(mnist.test.labels.shape)

    # define a neural network (softmax logistic regression)
    # input image: x and label: y
    x = tf.placeholder(tf.float32, [None, 784])
    y = tf.placeholder(tf.float32, [None, 10])

    W = tf.Variable(tf.zeros([784, 10]))
    b = tf.Variable(tf.zeros([10]))
    predict = tf.nn.softmax(tf.matmul(x, W) + b)  # the equation
    print(x)
    print(predict)

    # define the train step to minimize the cross entropy between y and prediction with SGD
    cross_entropy = -tf.reduce_sum(y * tf.log(predict))
    train_step = tf.train.GradientDescentOptimizer(0.01).minimize(cross_entropy)
    print(train_step)

    # initialize variables and session
    init = tf.global_variables_initializer()

    with tf.Session() as sess:
        sess.run(init)

        # train the model mini batch with 128 elements with 1000 iterations
        for i in range(1000):
            batch_xs, batch_ys = mnist.train.next_batch(128)
            _, loss = sess.run([train_step, cross_entropy], feed_dict={x: batch_xs, y: batch_ys})
            if i % 200 == 0:
                print("loss =", loss)

        # evaluate the accuracy of the model
        correct_prediction = tf.equal(tf.argmax(predict, 1), tf.argmax(y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
        print(sess.run(accuracy, feed_dict={x: mnist.test.images, y: mnist.test.labels}))


if __name__ == '__main__':
    main()