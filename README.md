# bmtensorflow

## Requirement
- NVIDIA driver 4XX above (for CUDA 10)
- nvidia-docker

## Docker Environment

Build docker image
```sh
$ cd {REPO_ROOT}/docker/gpu
$ sudo make build
```

Run docker image
```sh
$ cd {REPO_ROOT}/docker/gpu
$ sudo make bash
```

## Build Tensorflow 1.11

```sh
$ sh build_tensorflow.sh
```
