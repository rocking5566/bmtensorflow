#!/bin/bash
cd tensorflow-1.13.1

if [ -z "$1" ]; then
    echo "Nothing to do"
elif [ $1 = "test" ]; then
    echo "Run unit test for fake_quant_ops_test"
    bazel test --config=opt --copt=-mavx --config=cuda \
        --cxxopt="-D_GLIBCXX_USE_CXX11_ABI=0" \
        //tensorflow/core/kernels:fake_quant_ops_test
    bazel-bin/tensorflow/core/kernels/fake_quant_ops_test
elif [ $1 = "plot_tflite" ]; then
    echo "plot tflite model"
    bazel run --config=opt --copt=-mavx --config=cuda \
        --cxxopt="-D_GLIBCXX_USE_CXX11_ABI=0" \
        tensorflow/contrib/lite/tools:visualize -- \
    /workspace/visualize/mobilenet_v1_1.0_224/mobilenet_v1_1.0_224.tflite  /workspace/visualize/model_viz.html
fi

cd ..
