#!/bin/bash
pip uninstall -y tensorflow
cd tensorflow-1.13.1

if [ -z "$1" ]; then
    echo "Build without config (incremental build)"
    bazel build --config=opt --copt=-mavx --config=cuda \
        --cxxopt="-D_GLIBCXX_USE_CXX11_ABI=0" \
        tensorflow/tools/pip_package:build_pip_package
elif [ $1 = "rebuild" ]; then
    echo "Clean build"
    tensorflow/tools/ci_build/builds/configured GPU \
    bazel build --config=opt --copt=-mavx --config=cuda \
        --cxxopt="-D_GLIBCXX_USE_CXX11_ABI=0" \
        tensorflow/tools/pip_package:build_pip_package
fi

bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/pip
pip --no-cache-dir install --upgrade /tmp/pip/tensorflow-*.whl
cd ..
